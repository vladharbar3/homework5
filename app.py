from flask import Flask, request, make_response
import requests
import json
from faker import Faker
from sqlalchemy import text
import os
import sqlite3

app = Flask(__name__)

def execute_query(query, args=()):
    conn = sqlite3.connect(
        os.path.join(
            os.path.dirname(__file__),
            'chinook.db'
        )
    )
    cur = conn.cursor()
    cur.execute(query, args)
    result = cur.fetchall()
    conn.commit()
    conn.close()

    return result

@app.route('/bitcoin_rate')
def get_bitcoin_rate():
    currency = 'USD'
    if len(request.args.getlist('currency')) != 0:
        currency = request.args['currency']
    r = requests.get('https://bitpay.com/api/rates')
    r_list = json.loads(r.text)
    for i in r_list:
        if i['code'] == currency:
            response = make_response(f"Rate bitcoin to {currency} = {i['rate']}")
            response.headers['Content-Type'] = 'text/plain'
            return response
    return f"Not find {currency} currency"


@app.route('/random_users')
def gen_random_users():
    count = 100
    if len(request.args.getlist('count')) != 0:
        count = int(request.args['count'])
    fake = Faker()
    fake_list = []

    for _ in range(count):
        fake_list.append(f" name: {fake.name()}, email: {fake.ascii_company_email()}")

    response = make_response(
        json.dumps(fake_list, indent=2)
    )
    response.headers['Content-Type'] = 'text/plain'
    return response
    # return (f"Numbers:{new_line}{new_line.join(map(str, fake_list))}")

@app.route('/unique_names')
def get_unique_names():
    count_unique = execute_query("SELECT count(DISTINCT FirstName) AS AAA FROM customers")
    return f"Count of unique first name - {count_unique[0][0]}"

@app.route('/tracks_count')
def get_tracks_count():
    count_unique = execute_query("SELECT count(*) FROM tracks")
    return f"Count of records - {count_unique[0][0]}"

@app.route('/customers')
def get_customers():
    country= ''
    if len(request.args.getlist('Country')) != 0:
        country = '"' + request.args['Country'] + '"'
    city = ''
    if len(request.args.getlist('City')) != 0:
        city = '"' + request.args['City'] +'"'


    text_sql = "SELECT * FROM customers"

    if country != '' and city == '':
        text_sql = f"{text_sql} WHERE (Country={country})"
    elif country == '' and city != '':
        text_sql = f"{text_sql} WHERE (City={city})"
    elif country != '' and city != '':
        text_sql = f"SELECT * FROM customers WHERE (Country={country} and City={city})"



    count_unique = execute_query(text_sql)

    response = make_response(
        json.dumps(count_unique, indent=2)
    )
    response.headers['Content-Type'] = 'text/plain'
    return response

    # return f"Costomer for city = {city} and country = {country} "


if __name__ == '__main__':
    app.run(debug=True)
